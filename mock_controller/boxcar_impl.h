#pragma once

#include "uni_measurement_manager/measurement_device_impl.h"

namespace unim = Uni_measurement_manager;

namespace Boxcar_controller {

    class Boxcar_impl : public unim::Measurement_device_impl {
        public:
            class Boxcar_drive_impl : public unim::Measurement_device_impl::Drive_impl {
                public:
                    Boxcar_drive_impl(int id);
                    double min_position();
                    double max_position();
                    void move(double position);
            };
            Boxcar_impl();
            void connect();
            double read_out();
    };

}
