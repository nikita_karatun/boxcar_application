#include "boxcar_impl.h"

#include <iostream>

namespace Boxcar_controller {

    Boxcar_impl::Boxcar_impl() {
        add_drive(new Boxcar_drive_impl(1));
    }

    void Boxcar_impl::connect() { 
        std::cout << "connect" << std::endl;
    }

    double Boxcar_impl::read_out() { 
        return 1;
    }

    Boxcar_impl::Boxcar_drive_impl::Boxcar_drive_impl(int id) 
        :unim::Measurement_device_impl::Drive_impl(id)  
    {}

    double Boxcar_impl::Boxcar_drive_impl::min_position() { return 0; }

    double Boxcar_impl::Boxcar_drive_impl::max_position() { return 10; }

    void Boxcar_impl::Boxcar_drive_impl::move(double position) { 
        std::cout << "moved to " << position << std::endl;
    }

}
