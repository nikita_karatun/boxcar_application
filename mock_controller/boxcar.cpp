#include "boxcar_controller/boxcar.h"

#include "boxcar_impl.h"

namespace Boxcar_controller {

    Boxcar::Boxcar() {
        p_impl_ = new Boxcar_impl;
    }

    Boxcar::~Boxcar() {
        delete p_impl_;
    }

    void Boxcar::connect() {
        p_impl_->connect();
    }
    double Boxcar::read_out() {
        return p_impl_->read_out();
    }

    unim::Measurement_device::Drive::iterator_type Boxcar::drives_iterator() {
        return p_impl_->drives_iterator();
    }

}
