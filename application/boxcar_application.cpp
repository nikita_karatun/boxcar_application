#include "boxcar_application.h"

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "boxcar_controller/boxcar.h"

namespace Boxcar_application {

    std::auto_ptr<unim::Measurement_device> Device_factory::create_measurement_device() {
        return std::auto_ptr<unim::Measurement_device>(new Boxcar_controller::Boxcar);
    }

    std::auto_ptr<unim::Properties_provider> Context_factory::create_properties_provider() {
        std::auto_ptr<unim::Properties_provider_impl> properties_provider(
                new unim::Properties_provider_impl);
        properties_provider
            ->add_property("application.title", "Boxcar")
            ->add_property("panels.positioning.title", "Delay")
            ->add_property("output_file.header.x", "Delay (V)")
            ->add_property("output_file.header.y", "Value (V)"); // todo move add property method to interface?
        return std::auto_ptr<unim::Properties_provider>(properties_provider.release());
    }

    Boxcar_application::Boxcar_application(): log_stream_("boxcar.log", std::ofstream::out) {}

    bool Boxcar_application::OnInit() {
        wxLog* logger = new wxLogStream(&log_stream_);
        wxLog::SetActiveTarget(logger);
        return unim::Application_impl::OnInit();
    }

    std::auto_ptr<unim::Measurement_context_factory> Boxcar_application::create_measurement_context_factory() {
        return std::auto_ptr<unim::Measurement_context_factory>(new Context_factory);
    }

    std::auto_ptr<unim::Measurement_device_factory> Boxcar_application::create_measurement_device_factory() {
        return std::auto_ptr<unim::Measurement_device_factory>(new Device_factory);
    }

}

wxIMPLEMENT_APP(Boxcar_application::Boxcar_application);

