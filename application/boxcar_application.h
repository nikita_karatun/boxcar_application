#pragma once

#include "uni_measurement_manager/measurement_device.h"
#include "uni_measurement_manager/application_impl.h"
#include "uni_measurement_manager/measurement_context_factory_impl.h"
#include "uni_measurement_manager/hot_keys_handler_impl.h"
#include "uni_measurement_manager/properties_provider_impl.h"

namespace unim = Uni_measurement_manager;

namespace Boxcar_application {

    class Device_factory : public unim::Measurement_device_factory {
        public:
            std::auto_ptr<unim::Measurement_device> create_measurement_device();
    };

    class Context_factory : public unim::Measurement_context_factory_impl {
        public:
            std::auto_ptr<unim::Properties_provider> create_properties_provider();
    };

    class Boxcar_application : public unim::Application_impl {
        public:
            Boxcar_application();
            bool OnInit();
        protected:
            std::auto_ptr<unim::Measurement_context_factory> create_measurement_context_factory();
            std::auto_ptr<unim::Measurement_device_factory> create_measurement_device_factory();
        private:
            std::ofstream log_stream_;
    };

}
