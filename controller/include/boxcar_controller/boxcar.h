#pragma once

#include "uni_measurement_manager/measurement_device.h"

namespace unim = Uni_measurement_manager;

namespace Boxcar_controller {

    class Boxcar_impl;
    class Boxcar : public unim::Measurement_device {
        public:
            Boxcar();
            ~Boxcar();
            void connect();
            double read_out();
            unim::Measurement_device::Drive::iterator_type drives_iterator();
        private:
            Boxcar_impl* p_impl_;
    };

}
